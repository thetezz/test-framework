#!/usr/bin/env groovy
import hudson.model.*
import jenkins.util.*
import jenkins.model.*


node { 
        stage("Prepare Workspace") {
            deleteDir()
            checkout scm
        }
        
        stage("Test Lambda"){
            def testfile = 'playbooks/roles/testlambda/vars/main.yml'
            if (TEST_FILE != null && TEST_FILE != '')
                testfile = TEST_FILE
            def tests = readYaml(file: testfile)
            def envVarIncluders = []
            def fileStr = ''
            def i = 0

            for (test in tests.lambda_tests){
                def envVarIncluderData = []
                envVarIncluderData[0] = test.key
                for (testvalue in test.value){
                    if (testvalue.value != null && testvalue.value != ''){
                        if (testvalue.key == 'arn'){
                            envVarIncluderData[1] = testvalue.value
                        }
                        if (testvalue.key == 'region'){
                            envVarIncluderData[2] = testvalue.value
                        }
                        if (testvalue.key == 'payload'){
                            envVarIncluderData[3] = testvalue.value
                        }
                        if (testvalue.key == 'status'){
                            envVarIncluderData[4] = testvalue.value
                        }
                        if (testvalue.key == 'creds'){
                            envVarIncluderData[5] = testvalue.value
                        }
                    }
                }
                if (envVarIncluderData[1] != null) {//arn is defined
                    envVarIncluders[i] = envVarIncluderData
                    i++
                }
            }
           echo 'reviewing var includer data...'
            for (int x=0; x < envVarIncluders.size(); x++){
                def envVarIncluderData = envVarIncluders[x]
                for (int y=0; y < envVarIncluderData.size(); y++){
                   echo envVarIncluderData[y]
                }
            }
           echo 'reviewing var includer data...done'

            for (int x=0; x < envVarIncluders.size(); x++){
                def envVarIncluderData = envVarIncluders[x]
                echo 'Executing test: "' + envVarIncluderData[0] + '"'
                if (envVarIncluderData[1] == null) //arn should be defined
                    continue
                def envVarIncluder = "--extra-vars 'function_arn=" + envVarIncluderData[1]
                if (envVarIncluderData[2] != null){
                    envVarIncluder = envVarIncluder + " region=" + envVarIncluderData[2]
                }
                if (envVarIncluderData[3] != null){
                    envVarIncluder = envVarIncluder + " payload=" + envVarIncluderData[3]
                }
                if (envVarIncluderData[4] != null){
                    envVarIncluder = envVarIncluder + " status=" + envVarIncluderData[4]
                }
                if (envVarIncluderData[5] == null){
                    envVarIncluder = envVarIncluder + "'"
                     echo "itSS:" + envVarIncluder  

                  try {
                        sh """
                            ansible-playbook playbooks/testlambda.yml -v ${envVarIncluder}
                        """
                        fileStr = fileStr + envVarIncluderData[0] + ": PASSED\n"
                    } catch (Exception e) {
                        echo 'Exception in ' + envVarIncluderData[0] + ': ' + e.toString()
                        fileStr = fileStr + envVarIncluderData[0] + ": FAILED\n"
                    }
                }
                else {
                    try {
                        withCredentials([[
                        $class : 'UsernamePasswordMultiBinding',
                        credentialsId: envVarIncluderData[5],
                        usernameVariable: 'accessKey',
                        passwordVariable: 'secretKey']]) {
                            envVarIncluder = envVarIncluder + ' aws_access_key=' + accessKey + ' aws_secret_key=' + secretKey + "'"
                            sh """
                                ansible-playbook playbooks/testlambda.yml -v ${envVarIncluder}
                            """
                            fileStr = fileStr + envVarIncluderData[0] + ": PASSED\n"
                        }
                    } catch (Exception e){
                        echo 'Exception in ' + envVarIncluderData[0] + ': ' + e.toString()
                        fileStr = fileStr + envVarIncluderData[0] + ": FAILED\n"
                    }
                }
            }
            writeFile file:"${Workspace}/lambdaChk.file", text:fileStr
        }
}

