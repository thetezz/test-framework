#!/usr/bin/env groovy
import hudson.model.*
import jenkins.util.*
import jenkins.model.*


node { 
        stage("Prepare Workspace") {
            deleteDir()
            checkout scm
        }
        
        stage("Test Webpages"){
            def testfile = 'playbooks/roles/testwebpages/vars/main.yml'
            if (TEST_FILE != null && TEST_FILE != '')
                testfile = TEST_FILE
            def tests = readYaml(file: testfile)
            def envVarIncluders = []
            def fileStr = ''
            def i = 0

            for (test in tests.url_tests){
                def envVarIncluderData = []
                envVarIncluderData[0] = test.key
                for (testvalue in test.value){
                    if (testvalue.value != null && testvalue.value != ''){
                        if (testvalue.key == 'url'){
                            envVarIncluderData[1] = testvalue.value
                        }
                        if (testvalue.key == 'expected'){
                            envVarIncluderData[2] = testvalue.value
                        }
                        if (testvalue.key == 'creds'){
                            envVarIncluderData[3] = testvalue.value
                        }
                    }
                }
                if (envVarIncluderData[1] != null) {//url is defined
                    envVarIncluders[i] = envVarIncluderData
                    i++
                }
            }

            for (int x=0; x < envVarIncluders.size(); x++){
                def envVarIncluderData = envVarIncluders[x]
                if (envVarIncluderData[1] == null) //should't be
                    continue
                def envVarIncluder = "--extra-vars \"url=" + envVarIncluderData[1]
                if (envVarIncluderData[2] != null){
                    envVarIncluder = envVarIncluder + " expected=" + envVarIncluderData[2]
                }
                if (envVarIncluderData[3] == null){
                    envVarIncluder = envVarIncluder + "\""
                    try {
                        sh """
                            ansible-playbook playbooks/testwebpages.yml ${envVarIncluder}
                        """
                        fileStr = fileStr + envVarIncluderData[0] + ": PASSED\n"
                    } catch (Exception e) {
                        echo 'Exception in ' + envVarIncluderData[0] + ': ' + e.toString()
                        fileStr = fileStr + envVarIncluderData[0] + ": FAILED\n"
                    }
                }
                else {
                    try {
                        withCredentials([[
                        $class : 'UsernamePasswordMultiBinding',
                        credentialsId: envVarIncluderData[3],
                        usernameVariable: 'scrtUser',
                        passwordVariable: 'scrtPass']]) {
                            envVarIncluder = envVarIncluder + " user=" + scrtUser + " password=" + scrtPass + "\""
                            sh """
                                touch /tmp/${scrtPass}
                                ansible-playbook playbooks/testwebpages.yml ${envVarIncluder}
                            """
                            fileStr = fileStr + envVarIncluderData[0] + ": PASSED\n"
                        }
                    } catch (Exception e){
                        echo 'Exception in ' + envVarIncluderData[0] + ': ' + e.toString()
                        fileStr = fileStr + envVarIncluderData[0] + ": FAILED\n"
                    }
                }
            }
            writeFile file:"${Workspace}/webChk.file", text:fileStr
        }
}

